import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';

abstract class Device {
  final String name;
  final String model;
  final TargetPlatform platform;
  final MediaQueryData? landscape;
  final MediaQueryData? portrait;

  bool get canRotate => landscape != null && portrait != null;

  Future<bool> hasTouch() async {
    bool? hasTouch;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      hasTouch = await const MethodChannel('fastotv_device_info').invokeMethod('getTouch');
    } on PlatformException {
      hasTouch = false;
    }
    return hasTouch!;
  }

  const Device(
      {required this.name,
      required this.model,
      required this.platform,
      this.landscape,
      this.portrait});
}

class WindowsDevice extends Device {
  const WindowsDevice(
      {required String name,
      required String model,
      MediaQueryData? landscape,
      MediaQueryData? portrait})
      : super(
            name: name,
            model: model,
            platform: TargetPlatform.windows,
            landscape: landscape,
            portrait: portrait);
}

class LinuxDevice extends Device {
  const LinuxDevice(
      {required String name,
      required String model,
      MediaQueryData? landscape,
      MediaQueryData? portrait})
      : super(
            name: name,
            model: model,
            platform: TargetPlatform.linux,
            landscape: landscape,
            portrait: portrait);
}

class MacOSXDevice extends Device {
  const MacOSXDevice(
      {required String name,
      required String model,
      MediaQueryData? landscape,
      MediaQueryData? portrait})
      : super(
            name: name,
            model: model,
            platform: TargetPlatform.macOS,
            landscape: landscape,
            portrait: portrait);
}

class IOSDevice extends Device {
  const IOSDevice(
      {required String name,
      required String model,
      MediaQueryData? landscape,
      MediaQueryData? portrait})
      : super(
            name: name,
            model: model,
            platform: TargetPlatform.iOS,
            landscape: landscape,
            portrait: portrait);
}

class AndroidDevice extends Device {
  const AndroidDevice(
      {required String name,
      required String model,
      MediaQueryData? landscape,
      MediaQueryData? portrait})
      : super(
            name: name,
            model: model,
            platform: TargetPlatform.android,
            landscape: landscape,
            portrait: portrait);
}

class AndroidDeviceWithoutTouch extends AndroidDevice {
  const AndroidDeviceWithoutTouch(
      {required String name,
      required String model,
      MediaQueryData? landscape,
      MediaQueryData? portrait})
      : super(name: name, model: model, landscape: landscape, portrait: portrait);

  @override
  Future<bool> hasTouch() async {
    return false;
  }
}
